package batch3;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;


@Configuration
@EnableScheduling
@ComponentScan({"batch2.scheduler"})
public class SchedulingConfigurerImpl implements SchedulingConfigurer{
	
	/*@Autowired
	@Qualifier("contextDataSource")
    public DataSource dataSource;
	*/
	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		taskRegistrar.setScheduler(scheduler());
	}	

	@Bean
	public Executor scheduler() {
		return Executors.newScheduledThreadPool(2);
	}
	
	

}
