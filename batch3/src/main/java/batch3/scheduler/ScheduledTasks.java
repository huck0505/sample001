package batch3.scheduler;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import batch3.module3.TestModule;
import module4.TestModule4;


@Component
public class ScheduledTasks {

	//private static final Logger log = (Logger) LoggerFactory.getLogger(ScheduledTasks.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    
    
    @Scheduled(fixedRate = 1000)
    public synchronized void job1Scheduler() throws Exception { 
    	System.out.println("****************************************************************** [START]: "+ dateFormat.format(new Date()));
    	TestModule test = new TestModule();
    	test.test();
    	System.out.println("****************************************************************** [END]: "+ dateFormat.format(new Date()));
    }
    
	
    @Scheduled(fixedRate = 5000)
    public synchronized void job2Scheduler() throws Exception { 
    	System.out.println("22222 [START]: "+ dateFormat.format(new Date()));
    	TestModule4 test = new TestModule4();
    	test.test();
    	System.out.println("22222 [END]: "+ dateFormat.format(new Date()));
    }
	
}
